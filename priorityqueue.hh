/**
 * Języki i narzędzia programowania I 2015/2016
 * Zadanie 5
 *
 * Autorzy:
 * Tomasz Kępa <tk359746@students.mimuw.edu.pl>
 * Paweł Poskrobko <pawel.poskrobko@student.uw.edu.pl>
 */


#ifndef ___PRIORITY_QUEUE___
#define ___PRIORITY_QUEUE___

#include <exception>
#include <cstddef>
#include <set>
#include <map>
#include <utility>
#include <iostream>
#include <algorithm>


class PriorityQueueNotFoundException : public std::exception
{
public:
  virtual const char* what() const throw() {
    return "Key not found";
  }
};

class PriorityQueueEmptyException : public std::exception
{
public:
  virtual const char* what() const throw() {
    return "Empty queue";
  }
};


template<typename K, typename V>
class PriorityQueue
{
private:

  typedef typename std::multimap<K, V>::iterator qElem;
  typedef typename std::multiset<qElem, bool(*)(const qElem, const qElem)> qSet;

  // Map holding all pairs for fast value changing
  std::multimap<K, V> mapping;

  // Set detemininig the order of the minSet for min and max operations
  qSet minSet;

  // Set determining the order of the minSet for queues comparison
  qSet compSet;

  void deleteMinMax(typename qSet::iterator minSetIter) {
    // first erase may throw, but still strong guarantee
    compSet.erase(*minSetIter);
    mapping.erase(*minSetIter);
    minSet.erase(minSetIter);
  }

  static bool lessByValue (const qElem lhs, const qElem rhs) {
    if (!(lhs->second < rhs->second) && !(rhs->second < lhs->second)) {
      return lhs->first < rhs->first;
    } else {
      return lhs->second < rhs->second;
    }
  }

  static bool lessByKey (const qElem lhs, const qElem rhs) {
    if (!(lhs->first < rhs->first) && !(rhs->first < lhs->first)) {
      return lhs->second < rhs->second;
    } else {
      return lhs->first < rhs->first;
    }
  }


public:

  // Types definitions
  typedef size_t size_type;
  typedef K key_type;
  typedef V value_type;

  // Default ctor
  PriorityQueue() :
    mapping(),
    minSet(lessByValue),
    compSet(lessByKey)
  {}

  // Copy ctor [O(n log(n) where n = queue.size())]
  PriorityQueue(const PriorityQueue<K, V>& queue) :
    mapping(queue.mapping),
    minSet(lessByValue),
    compSet(lessByKey)
  {
    for (qElem i = mapping.begin(), end = mapping.end(); i != end; i++) {
      minSet.insert(i);
      compSet.insert(i);
    }
  }

  // Move ctor [O(1)]
  PriorityQueue(PriorityQueue<K, V>&& queue) :
    mapping(std::move(queue.mapping)),
    minSet(std::move(queue.minSet)),
    compSet(std::move(queue.compSet))
  {}

  // Copy assingment operator [O(n log(n) where n = queue.size())]
  PriorityQueue<K, V>& operator=(const PriorityQueue<K, V>& queue) {
    if (this != &queue) {
      PriorityQueue<K, V> copy(queue);
      mapping = std::move(copy.mapping);
      minSet = std::move(copy.minSet);
      compSet = std::move(copy.compSet);
    }
    return *this;
  }

  // Move assingment operator [O(1)]
  PriorityQueue<K, V>& operator=(PriorityQueue<K, V>&& queue) {
    mapping = std::move(queue.mapping);
    minSet = std::move(queue.minSet);
    compSet = std::move(queue.compSet);
    return *this;
  }

  // Checks the empiness of the queue [O(1)]
  bool empty() const {
    return size() == 0;
  }

  // Return the count of the (key, value) pairs hold in the queue [O(1)]
  size_type size() const {
    return mapping.size();
  }

  // Inserts (key, value) pair into the queue [O(log size())]
  void insert(const K& key, const V& value) {
    qElem result = mapping.insert(std::pair<K, V>(key, value));
    try {
      auto minSetIter = minSet.insert(result);
      try {
        compSet.insert(result);
      } catch(...) {
        minSet.erase(minSetIter); // no-throw
        throw;
      }
    } catch(...) {
      mapping.erase(result); // no-throw
      throw;
    }
  }

  // Returns the minimum value hold in the queue [O(1)]
  const V& minValue() const {
    if (empty()) {
      throw PriorityQueueEmptyException();
    }
    return (*(minSet.cbegin()))->second;
  }

  // Returns the maximum value hold in the queue [O(1)]
  const V& maxValue() const {
    if (empty()) {
      throw PriorityQueueEmptyException();
    }

    auto it = minSet.cend();
    return (*(--it))->second;
  }

  // Returns the key of the minimum value hold in the queue [O(1)]
  const K& minKey() const {
    if (empty()) {
      throw PriorityQueueEmptyException();
    }
    return (*(minSet.cbegin()))->first;
  }

  // Returns the key of the maximum value hold in the queue [O(1)]
  const K& maxKey() const {
    if (empty()) {
      throw PriorityQueueEmptyException();
    }

    auto it = minSet.cend();
    return (*(--it))->first;
  }

  // Removes the pair with the minimum value from the queue [O(log size())]
  void deleteMin() {
    if (!empty()) {
      deleteMinMax(minSet.begin());
    }
  }

  // Removes the pair with the maximum value from the queue [O(log size())]
  void deleteMax() {
    if (!empty()) {
      auto it = minSet.end();
      deleteMinMax(--it);
    }
  }

  // Changes the value assigned to the given key  [O(log size())]
  void changeValue(const K& key, const V& value) {

    // find the key-value pair
    qElem it = mapping.find(key);
    if (it == mapping.end()) {
      throw PriorityQueueNotFoundException();
    }

    // using find before modifications for strong guarantee
    auto minSetIter = minSet.find(it);
    auto compSetIter = compSet.find(it);

    // inserting new pair
    insert(key, value);

    // after successful insertion, no-throw erases based on iterators
    minSet.erase(minSetIter);
    compSet.erase(compSetIter);
    mapping.erase(it);
  }

  // Merges two queues and leaves the second queue empty [O(size() + queue.size() * log (queue.size() + size()))]
  void merge(PriorityQueue<K, V>& queue) {
    if (this != &queue) {
      PriorityQueue<K, V> temp(*this);
      PriorityQueue<K, V> argCopy(queue);

      while (!argCopy.empty()) {
        temp.insert(argCopy.minKey(), argCopy.minValue());
        argCopy.deleteMin();
      }

      queue.swap(argCopy);
      (*this).swap(temp);
    }    
  }

  // Swaps the constets of two queues [O(1)]
  void swap(PriorityQueue<K, V>& queue) {
    std::swap(mapping, queue.mapping);
    std::swap(minSet, queue.minSet);
    std::swap(compSet, queue.compSet);
  }

  // Friends functions delarations
  template<typename Key, typename Value>
  friend void swap(PriorityQueue<Key,Value>& lhs,
                   PriorityQueue<Key,Value>& rhs);

  template<typename Key, typename Value>
  friend bool operator<(const PriorityQueue<Key, Value>& lhs,
                        const PriorityQueue<Key, Value>& rhs);
};

// Swaps the constets of two queues [O(1)]
template<typename K, typename V>
void swap(PriorityQueue<K, V>& lhs, PriorityQueue<K, V>& rhs) {
  std::swap(lhs.mapping, rhs.mapping);
  std::swap(lhs.minSet, rhs.minSet);
  std::swap(lhs.compSet, rhs.compSet);
}

// Comparison operators

template<typename K, typename V>
bool operator<(const PriorityQueue<K, V>& lhs, const PriorityQueue<K, V>& rhs) {
  bool result =  std::lexicographical_compare(lhs.compSet.cbegin(), lhs.compSet.cend(),
        rhs.compSet.cbegin(), rhs.compSet.cend(), lhs.lessByKey);
  return result;
}

template<typename K, typename V>
bool operator==(const PriorityQueue<K, V>& lhs, const PriorityQueue<K, V>& rhs) {
  return !(lhs < rhs) && !(rhs < lhs);
}

template<typename K, typename V>
bool operator!=(const PriorityQueue<K, V>& lhs, const PriorityQueue<K, V>& rhs) {
  return !(lhs == rhs);
}

template<typename K, typename V>
bool operator>(const PriorityQueue<K, V>& lhs, const PriorityQueue<K, V>& rhs){
  return !(lhs < rhs) && lhs != rhs;
}

template<typename K, typename V>
bool operator<=(const PriorityQueue<K, V>& lhs, const PriorityQueue<K, V>& rhs){
  return !(lhs > rhs);
}

template<typename K, typename V>
bool operator>=(const PriorityQueue<K, V>& lhs, const PriorityQueue<K, V>& rhs){
  return !(lhs < rhs);
}

#endif
