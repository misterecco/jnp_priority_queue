#include <iostream>
#include <exception>
#include <cassert>

#include "priorityqueue.hh"

int main() {
	std::cout << "START" << std::endl;

  PriorityQueue<int, int> T;
  T.insert(1, 1);
  T.insert(1, 2);
  T.insert(1, 10);
  T.insert(1, 5);
  T.insert(1, 3);
  T.insert(2, 4);
  T.insert(3, 9);
  T.insert(4, 16);
  // T.printInfo();
  PriorityQueue<int, int> S;
  S.insert(1, 1);
  S.insert(1, 10);
  S.insert(1, 2);
  S.insert(1, 5);
  S.insert(1, 3);
  S.insert(2, 4);
  S.insert(3, 9);
  S.insert(4, 16);
  // S.printInfo();

  assert(T == S);
  S.deleteMax();
  assert(T != S);
  assert(S < T);
  S.deleteMin();
  assert(S > T);

  std::cout << "ALL OK!" << std::endl;
  return 0;
}
