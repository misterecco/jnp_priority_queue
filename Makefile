CC = g++
CFLAGS = -Wall -std=c++11 -O2 -c -fsanitize=address
LDFLAGS = -Wall -std=c++11 -O2 -fsanitize=address

OBJECTS =
ALL = example test test_pp

all: $(ALL)

%.o : %.cc
	$(CC) $(CFLAGS) $<

$(ALL) : % : %.o $(OBJECTS)
	$(CC) $(LDFLAGS) -o $@ $^

clean:
	rm -f *.o $(ALL) *~
